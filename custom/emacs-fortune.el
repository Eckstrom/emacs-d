;; emacs-fortune.el

;; Author: Joseph Eckstrom
;; Version: 2018.11.17 (originally 2015.07.08)

;;; Commentary:

;; This is a fun little thing for emacs, it basically runs bsd fortune on systems
;; where fortune may not exit. It will print a random fortune in the message buffer.
;;

(require 'utils)

(defun get-file-content-list (file)
  "Grab the contents of file and split it into a list"
  (with-temp-buffer
    (insert-file-contents file)
    (split-string (buffer-string) "\n%\n" t)))


(defun get-random-element (lst)
  "Get a random element from LST"
  (nth (random (length lst)) lst))


(defun get-random-file (directory)
    "Select a random file from directory (excluding . and ..)"
    (get-random-element
     (directory-files-recursively directory "^\\(?:[^\\.]+$\\)")))

 
(defun emacs-fortune ()
  "Get a fortune from one of our fortune files"
  (if (executable-find "fortune")
      (message (shell-command "fortune"))
    (let ((fdir "~/.emacs.d/fortunes/"))
      (get-random-element (get-file-content-list (get-random-file fdir))))))


(defun tell-fortune ()
  (interactive)
  (message (emacs-fortune)))


(provide 'emacs-fortune)
