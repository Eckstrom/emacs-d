;; init.el

;; Author: Joseph Eckstrom
;; Version: 2018.11.17 (originally 2015.07.08)

;;; Commentary:

;; Searches through files to find strings (think a dumb grep), and prints the
;; results to the *search-results* buffer
;;
(require 'utils)


(defvar results-buffer)
(setq results-buffer "*search-results*")


(defun string-trim (str)
  "Chomp leading and tailing whitespace from STR."
  (replace-regexp-in-string (rx (or (: bos (* (any " \t\n")))
                                    (: (* (any " \t\n")) eos)))
                            ""
                            str))


(defun get-result (fname)
  "Pretty-print the result from the match"
  (let ((lnum  (count-lines 1 (point)))
        (fnam  (file-name-nondirectory fname))
        (start (line-beginning-position)))
    (let ((result (string-trim
                   (buffer-substring start (point)))))
      (format "[%s:%d] %s...\n" fnam lnum result))))


(defun search-file (matcher pattern fname)
  "Search a file for a specific pattern."
  (with-temp-buffer
    (progn
      (insert-file-contents fname)
      (while (funcall matcher pattern nil t)
        (let ((pretty-result (get-result fname)))
          (with-current-buffer results-buffer
            (insert pretty-result)))))))


;re-search-forward
(defun get-directory-contents (dir)
  "Get all files in dir, recursively"
  (directory-files-recursively dir ""))


(defun open-grep-results-buffer ()
  "Open, clear, and display the results-buffer"
  (let ((rb (get-buffer-create results-buffer)))
    (progn
      (display-buffer rb)
      (with-current-buffer rb
        (erase-buffer))
      rb)))


(defmacro grep-file-with-results (&rest body)
  `(progn
     (open-grep-results-buffer)
     (progn ,@body)
     (with-current-buffer results-buffer
       (insert "// END RESULTS\n"))))


(defun re-search-file (pattern file)
  "Search for a regex in a single file"
  (interactive "sPattern to search for:\nfFile to look in:")
  (grep-file-with-results
    (search-file 're-search-forward pattern file)))


(defun re-search-dir (pattern dir &optional ffilter)
  "Search for a regex in every file in a directory"
  (interactive "sPattern to search for:\nDDirectory to look in:")
  (grep-file-with-results
   (dolist (file (get-directory-contents dir))
     (search-file 're-search-forward pattern file))))


(defun find-in-file (pattern file)
  "Search for a pattern in a single file"
  (interactive "sPattern to search for:\nfFile to look in:")
  (grep-file-with-results
    (search-file 'search-forward pattern file)))


(defun find-in-dir (pattern dir &optional ffilter)
  "Search for a pattern in every file in a directory"
  (interactive "sPattern to search for:\nDDirectory to look in:")
  (grep-file-with-results
   (dolist (file (get-directory-contents dir))
     (search-file 'search-forward pattern file))))


(provide 'find)
 
