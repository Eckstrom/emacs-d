;ELC   
;;; Compiled
;;; in Emacs version 26.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.

;;; This file does not contain utf-8 non-ASCII characters,
;;; and so can be loaded in Emacs versions earlier than 23.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\300\301!\210\300\302!\207" [require thingatpt utils] 2)
(defalias 'indent-whole-buffer #[nil "\212\300ed\301#)\207" [indent-region nil] 4 nil nil])
(defalias 'get-word-boundaries #[nil "\301\302!\211\205 @ATD)\207" [bounds bounds-of-thing-at-point word] 3 nil nil])
(defalias 'get-current-boundaries #[nil "\300 \203\f \301 \302 TD\207\303 \207" [use-region-p region-beginning region-end get-word-boundaries] 2 nil nil])
(defalias 'quotify-word #[(qt-char start stop) "\212b\210	c\210\nb\210	c)\207" [start qt-char stop] 1 nil nil])
(defalias 'quotify-boundaries #[(&optional single-quote\?) "\203 \305\202	 \306\307 \211@\nA@\205! \f\205! \310	\f#,\207" [single-quote\? qt #1=#:--cl-var-- start stop "'" "\"" get-current-boundaries quotify-word] 5 nil nil])
(defalias 'get-line-or-region-boundaries #[nil "\300 \203 \301 \302 D\207\303 \304 D\207" [use-region-p region-beginning region-end line-beginning-position line-end-position] 2])
(defalias 'quotify-each-word #[(&optional single-quote\?) "\203 \306\202	 \307\310 \211@\nA@\311\312\211\f\"-\207" [single-quote\? qt #1=#:--cl-var-- start stop #2=#:--cl-do-quotify-- "'" "\"" get-line-or-region-boundaries nil #[(start stop) "\212b\210\306v\210\307 \211@	A@\n\205, \205, \n\fW\205, \310\n#\210\n\311\f\\\",\207" [start #3=#:--cl-var-- beg end stop qt nil get-word-boundaries quotify-word 2 #2#] 5]] 4 nil nil])
#@56 Delete the current line. Don't add it to the kill-ring
(defalias 'kill-line-nocopy #[nil "\300y\210`\301y\210`|\207" [0 nil] 2 (#$ . 1819) nil])
#@64 Kill the previous N lines, beginning above the cursor position
(defalias 'kill-previous-lines #[(n) "\301V\205 \302\212[y\210\303 )S\303 S\"\207" [n 0 kill-region line-beginning-position] 3 (#$ . 1970) "NHow many previous lines: "])
#@60 Kill the next N lines, beginning below the cursor position
(defalias 'kill-next-lines #[(n) "\301V\205 \302\303 T\212y\210\303 )T\"\207" [n 0 kill-region line-end-position] 3 (#$ . 2212) "NHow many lines: "])
(defalias 'goto-new-line-above #[nil "\300\210\301 \207" [0 newline-and-indent] 1 nil nil])
(defalias 'open-line-above #[nil "\212\300 )\207" [goto-new-line-above] 1 nil nil])
(defalias 'kill-to-beginning #[nil "\300\301 `\"\207" [kill-region line-beginning-position] 3 nil nil])
(defalias 'kill-to-end #[nil "\300`\301 \"\207" [kill-region line-end-position] 3 nil nil])
(defalias 'paste-above #[nil "\212\300\210\301 \210\302 )\207" [0 newline yank] 1 nil nil])
(defalias 'paste-below #[nil "\212\300\301!\210\302 )\207" [beginning-of-line 2 yank] 2 nil nil])
(defalias 'paste-at #[(n) "\212\210\301 \210\302 )\207" [n newline yank] 1 nil "NWhere to paste: "])
(defalias 'make-new-empty-file #[(filename) "\301\302\303\304$\207" [filename write-region "" nil t] 5 nil "pFile: "])
#@58 kill all buffers except this one, and unsplit the window
(defalias 'kill-other-buffers #[nil "\300\301\302p\303 \"\"\210\304 \210\305 \207" [mapc kill-buffer delq buffer-list delete-other-windows delete-other-frames] 5 (#$ . 3217) nil])
#@71 Just like kill region, but if there isn't a region, it kills the line
(defalias 'kill-region-rmp #[nil "\301\302 \203 \303\304 \305 \"\202 \303\306 \306\307!\")\207" [select-enable-clipboard t use-region-p kill-region region-beginning region-end line-beginning-position 2] 4 (#$ . 3460) nil])
(defalias 'keep-matching-lines #[(regexp) "\212\301b\210\302!)\207" [regexp 0 keep-lines] 2 nil (list (read-regexp "Keep lines containing match for regexp"))])
(defalias 'keep-unique-lines #[(&optional ignore-whitespace) "\306\307\310\311\"\312b\210	\205m \313\302\306\"\203 \314\n!\202 \n)\315\f\316#\203E \317 \210\320\f\316\306\321#)\266\203\203b \322\323\324\f\"!\210\202b \320\f\316\306\321#)\266\203\203\\ \322\323\325\f\"!\210\326\f\306#\210)\316y\312U\211\204 \316*\207" [ht more-lines line ignore-whitespace this-line inhibit-changing-match-data t make-hash-table :test equal 0 thing-at-point string-trim gethash nil kill-line-nocopy ";;;" string-match print format "'%s' was already in the hash-table" "'%s' isn't the same as the ';;;' already in the hash-table" puthash] 8 nil (let ((answer (y-or-n-p "Ignore whitespace?"))) (list answer))])
#@81 comment-region or comment-region if there is a region. Otherwise, just the line
(defalias 'comment-or-uncomment #[nil "\300\301\302 \203 \303 \304 D\202 \305 \306 D\"\207" [apply comment-or-uncomment-region region-active-p region-beginning region-end line-beginning-position line-end-position] 4 (#$ . 4635) nil])
(defalias 'rename-file #[(file-name) "\302 \303	!\203 \304\305!\203 \306	!\210\205 \307!)\207" [current file-name buffer-file-name file-exists-p y-or-n-p "Overwrite existing file?" write-file delete-file] 2 nil "PRename to: "])
(provide 'emacs-keybindings)
