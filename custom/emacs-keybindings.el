;;;; emacs-keybindings.el

;;;; Author: Joseph Eckstrom
;;;; Version: 2018.12.10 (originally 2015.07.08)

;;;; Commentary:
;;;; Defines functions for keybindings described in init.el
;;;; provides emacs-keybindings.el

(require 'thingatpt)
;(require 'subr-x)
(require 'utils)

(eval-when-compile
  (defun emacs-version-24.4 ()
    (let ((major emacs-major-version)
          (minor emacs-minor-version))
      (or (> major 24) (and (= major 24) (>= minor 24)))))

  (if (emacs-version-24.4)
      (require 'subr-x)
    (defsubst string-trim-left (string &optional regexp)
      "Trim STRING of leading string matching REGEXP.
REGEXP defaults to \"[ \\t\\n\\r]+\"."
      (if (string-match (concat "\\`\\(?:" (or regexp "[ \t\n\r]+") "\\)") string)
          (substring string (match-end 0))
        string))

    (defsubst string-trim-right (string &optional regexp)
      "Trim STRING of trailing string matching REGEXP.
REGEXP defaults to  \"[ \\t\\n\\r]+\"."
      (let ((i (string-match-p (concat "\\(?:" (or regexp "[ \t\n\r]+") "\\)\\'")
                               string)))
        (if i (substring string 0 i) string)))

    (defsubst string-trim (string &optional trim-left trim-right)
      "Trim STRING of leading and trailing strings matching TRIM-LEFT and TRIM-RIGHT.
TRIM-LEFT and TRIM-RIGHT default to \"[ \\t\\n\\r]+\"."
      (string-trim-left (string-trim-right string trim-right) trim-left))))

;;; BEGIN: INDENT
;;;
;; indent the entire buffer, per the major mode specifications
;; -----------------------------------------------------------
(defun indent-whole-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))
;;;
;;; END: INDENT

;;; BEGIN: FIELDS
;;;
;;
;; -----

;;;
;;; END

;;; BEGIN: QUOTIFY
;;;
;; get the boundaries of the word under the cursor
;; -----------------------------------------------
(defun get-word-boundaries ()
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'word)))
    (when bounds
      (cl-values (car bounds) (1+ (cdr bounds))))))

;; get the boundaries of the word under the cursor, or the selected region
;; -----------------------------------------------------------------------
(defun get-current-boundaries ()
  (interactive)
  (if (use-region-p)
      (cl-values (region-beginning) (1+ (region-end)))
    (get-word-boundaries)))

;; quote the word, beginning at start, and ending at stop
;; ------------------------------------------------------
(defun quotify-word (qt-char start stop)
  (interactive)
  (save-excursion
    (goto-char start) (insert qt-char)
    (goto-char stop)  (insert qt-char)))

;; wrap the word under the cursor, or the selected region, in quotes
;; -----------------------------------------------------------------
(defun quotify-boundaries (&optional single-quote?)
  (interactive)
  (let ((qt (if single-quote? "'" "\"")))
    (cl-multiple-value-bind (start stop)
        (get-current-boundaries)
      (when (and start stop)
        (quotify-word qt start stop)))))

;; retrieve the boundaries of the selected region or current line
;; --------------------------------------------------------------
(defun get-line-or-region-boundaries ()
  (if (use-region-p)
      (cl-values (region-beginning) (region-end))
    (cl-values (line-beginning-position) (line-end-position))))

;; wrap each word in the selected region or current line in quotes
;; ---------------------------------------------------------------
(defun quotify-each-word (&optional single-quote?)
  (interactive)
  (let ((qt (if single-quote? "'" "\"")))
    (cl-multiple-value-bind (start stop)
        (get-line-or-region-boundaries)
      (nlet do-quotify ((start start) (stop stop))
        (save-excursion
          (goto-char start)
          (forward-word)
          (cl-multiple-value-bind (beg end)
              (get-word-boundaries)
            (when (and beg end (< beg stop))
              (quotify-word qt beg end)
              (do-quotify end (+ 2 stop)))))))))

;;;
;;; END: QUOTIFY

;;; BEGIN: KILL
;; kill line (without copying it)
;; ------------------------------
(defun kill-line-nocopy ()
  "Delete the current line. Don't add it to the kill-ring"
  (interactive)
  (delete-region (progn (forward-line 0) (point))
                 (progn (forward-line) (point))))


;; kill n previous lines
;; ---------------------
(defun kill-previous-lines (n)
  "Kill the previous N lines, beginning above the cursor position"
  (interactive "NHow many previous lines: ")
  (when (> n 0)
    (kill-region
     (1- (save-excursion
           (forward-line (- n))
           (line-beginning-position)))
     (1- (line-beginning-position)))))


;; kill next n lines
;; -----------------
(defun kill-next-lines (n)
  "Kill the next N lines, beginning below the cursor position"
  (interactive "NHow many lines: ")
  (when (> n 0)
    (kill-region
     (1+ (line-end-position))
     (1+ (save-excursion
           (forward-line n)
           (line-end-position))))))


;; open a line above the cursor and stay there
;; -------------------------------------------
(defun goto-new-line-above ()
  (interactive)
  (end-of-line 0)
  (newline-and-indent))


;; open a line above the cursor and come home
;; ------------------------------------------
(defun open-line-above ()
  (interactive)
  (save-excursion
    ;; use save-excursion to restore the cursor position
    (goto-new-line-above)))


;; kill from the current point to the beginning of the line
;; --------------------------------------------------------
(defun kill-to-beginning ()
  (interactive)
  (kill-region (line-beginning-position) (point)))


;; kill from the current point to the end of the line
;; --------------------------------------------------
(defun kill-to-end ()
  (interactive)
  (kill-region (point) (line-end-position)))


;; paste above the cursor
;; ----------------------
(defun paste-above ()
  (interactive)
  (save-excursion
    (end-of-line 0)
    (newline)
    (yank)))


;; paste below the cursor
;; ----------------------
(defun paste-below ()
  (interactive)
  (save-excursion
    (beginning-of-line 2)
    (yank)))


;; paste N lines above/below the cursor
;; ------------------------------------
(defun paste-at (n)
  (interactive "NWhere to paste: ")
  (save-excursion
    (end-of-line n)
    (newline)
    (yank)))

;; file operations
;; ---------------
(defun make-new-empty-file (filename)
  (interactive "pFile: ")
  (write-region "" nil filename t))


;; kill all other buffers
;; ----------------------
 (defun kill-other-buffers()
   "kill all buffers except this one, and unsplit the window"
   (interactive)
   (mapc 'kill-buffer (delq (current-buffer) (buffer-list)))
   (delete-other-windows)
   (delete-other-frames))


;; kill region remap
;; -----------------
(defun kill-region-rmp ()
  "Just like kill region, but if there isn't a region, it kills the line"
  (interactive)
  (let ((select-enable-clipboard t))
    (if (use-region-p)
        (kill-region (region-beginning) (region-end))
      (kill-region (line-beginning-position) (line-beginning-position 2)))))


;; keep only lines matching the given regex
;; ----------------------------------------
(defun keep-matching-lines (regexp)
  (interactive
   (list (read-regexp "Keep lines containing match for regexp")))
  (save-excursion
    (goto-char 0)
    (keep-lines regexp)))


;; go through a file, and delete any lines that are duplicates of previous lines
;; -----------------------------------------------------------------------------
(defun keep-unique-lines (&optional ignore-whitespace)
  (interactive (let ((answer (y-or-n-p "Ignore whitespace?")))
                 (list answer)))
  (let ((more-lines t)
        (ht (make-hash-table :test 'equal)))
    (goto-char 0)
    (while more-lines
      (let ((this-line (let ((line (thing-at-point 'line)))
                         (if ignore-whitespace
                             (string-trim line)
                           line))))
        (if (gethash this-line ht nil)
            (progn
              (kill-line-nocopy)
              (when (string-match-p (regexp-quote ";;;") this-line)
                (print (format "'%s' was already in the hash-table" this-line))))
          (progn
            (when (string-match-p (regexp-quote ";;;") this-line)
              (print (format "'%s' isn't the same as the ';;;' already in the hash-table" this-line)))
            (puthash this-line t ht))))
      (setf more-lines (zerop (forward-line))))))

;;;
;;; END: KILL

;;; BEGIN: COMMENTING
;;;
;; comment or uncomment region
;; ---------------------------
(defun comment-or-uncomment ()
  "comment-region or comment-region if there is a region. Otherwise, just the line"
  (interactive)
  (apply 'comment-or-uncomment-region
         (if (region-active-p)
             (list (region-beginning) (region-end))
           (list (line-beginning-position) (line-end-position)))))

;;;
;;; END: COMMENTING

;;; BEGIN: MOVE-FILE
;;;
;; rename the current buffer to the given file name
;; -------------------------------------------------------------------------
(defun rename-current-buffer-file (file-name)
  "Rename the current buffer to the given file-name. Creates a new file if file-name does not exist. Removes the old buffer-file if it exists."
  (interactive "FRename file to: ")
  (let ((current (buffer-file-name)))
    (when (or (not (file-exists-p file-name))
              (y-or-n-p "Overwrite existing file?"))
      (write-file file-name))
    (when current
      (delete-file current))))
;;;
;;; END: MOVE-FILE

;;; BEGIN: REVERT-BUFFER-NOCONFIRM
;;;
;; reread the buffer from disk
;; -------------------------------------------------------------------------
(defun revert-buffer-noconfirm ()
  "Reread the current buffer from disk if it has changed"
  (interactive)
  (when (buffer-modified-p)
    (revert-buffer :ignore-auto
                   :noconfirm
                   :preserve-modes)))
;;;
;;; END: MOVE-FILE

(provide 'emacs-keybindings)
