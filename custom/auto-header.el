;; auto-header.el

;; Author: Joseph Eckstrom
;; Version: 2019.01.28 (originally 2015.07.08)

;;; Commentary:

;; Auto-header provides the basic input for newly-created files that follow
;; specific mode or line-ending rules, as defined in the car of the
;; define-auto-insert expression(s). 

;; Miscellaneous settings & Requirements
;; -------------------------------------


(require 'autoinsert)

(defmacro get-auto-insert (comment-open comment-close comment-mid)
  `'("\n"
     ,(concat comment-open "\n" comment-mid " File: ")
     (file-name-nondirectory (buffer-file-name))
     ,(concat "\n"
              comment-mid " Author: Joey Eckstrom <jeckstrom135@gmail.com>\n"
              comment-mid " Maintainer: Joey Eckstrom <jeckstrom135@gmail.com>\n"
              comment-mid " Created: ")
     (format-time-string "%Y-%m-%d")
     ,(concat "\n" comment-mid " Copyright (C) ")
     (format-time-string "%Y")
     ,(concat ", Joey Eckstrom, all rights reserved.\n"
              comment-mid " Version: 0.0.1\n" comment-mid " Last Updated: ")
     (format-time-string "%Y-%m-%d")
     ,(concat "\n" comment-mid "           By: Joey Eckstrom\n"
              comment-mid "       Change: Initial creation\n"
              comment-mid "\n" comment-mid " Commentary:\n"
              comment-mid "  \n" comment-close "\n\n")
     > _ \n \n
     ,(concat comment-open "\n" comment-mid " end ")
     (file-name-nondirectory (buffer-file-name))
     ,(concat "\n" comment-close)))


(define-auto-insert
  '(lisp-mode . "Common Lisp Header")
  (get-auto-insert ";;;;" ";;;;" ";;;;"))

(define-auto-insert
  '("\\.[cCdhH][pPxX]*\\'" . "C/C++/D Header")
  (get-auto-insert "/*" "*/" " *"))


(define-auto-insert
  '("\\.\\(awk\\|sh\\|bash\\|pl\\)\\'" . "Shell Script")
  (get-auto-insert "###" "###" "###"))


(provide 'auto-header)
