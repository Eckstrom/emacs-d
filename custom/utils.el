;; init.el

;; Author: Joseph Eckstrom
;; Version: 2018.11.19 (originally 2015.07.08)

;;; Commentary:

;; Various utility functions and macros used throughout the startup scripts
;;

(require 'cl-lib)

(defmacro nlet (fname args &rest body)
  "NLET is a macro that defines a closure that can take default arguments,
and then executes the function given the default arguments provided.
For example, the below example function may expand as:

(defun fib (x)
  (nlet fib-recurse ((n x))
    (if (<= n 1) 0 (+ (fib (1- n)) (fib (- n 2))))))

==> (defun fib (x)
      (cl-labels ((fib-recurse (n)
                    (if (<= n 1) 
                        0 
                     (+ (fib-recurse (1- n)) (fib-recurse (- n 2))))))
        (fib-recurse x)))
"
  `(cl-labels ((,fname ,(mapcar 'car args)
                  ,@body))
     (,fname ,@(mapcar 'cadr args))))


(defun appendf (list1 list2)
  "Append LIST2 to LIST1, and assign the result to LIST1"
  (setf list1 (append list1 list2)))


(defun addend (item list)
  "Copy LIST, and add ITEM to the end of the copy"
  (reverse (cons item (reverse list))))


(defun addendf (item list)
  "Add ITEM to the end of LIST, modifying LIST"
  (nreverse (cons item (nreverse list))))


(defun --utils-seq-every-p (predicate sequence)
  (let ((end (length sequence)))
    (not
     (null
      (nlet get-truth ((idx 0))
        (when (< idx end)
          (and (funcall predicate (elt idx sequence))
               (get-truth (1+ idx)))))))))
    
(defun --utils-seq-some (predicate sequence)
  (let ((end (length sequence)))
    (nlet get-truth ((idx 0))
      (when (< idx end)
        (or (funcall predicate (elt idx sequence))
            (get-truth (1+ idx)))))))

(eval-when-compile
  (unless (functionp 'seq-every-p)
    (defalias 'seq-every-p '--utils-seq-every-p))
  (unless (functionp 'seq-some)
    (defalias 'seq-some '--utils-seq-some)))


(defun all (seq)
  "Is everything in seq non-nil?"
  (seq-every-p #'identity seq))


(defun any (seq)
  "Is anything in seq non-nil?"
  (seq-some #'identity seq))


(defun none (seq)
  "Is everything in seq nil?"
  (not (seq-some #'identity seq)))


(defun all-are (predicate &rest args)
  "Does applying the PREDICATE to every ARGS return only non-nil values?"
  (all (mapcar predicate args)))


(defun any-are (predicate &rest args)
  "Does applying the PREDICATE to every ARGS return any non-nil values?"
  (any (mapcar predicate args)))


(defun none-are (predicate &rest args)
  "Does applying the PREDICATE to every ARGS return only nil values?"
  (none (mapcar predicate args)))


(defmacro let-if (bindings then &optional else)
  "First, evaluate the BINDINGS. If all of them are non-nil, execute 
THEN within the lexical scope of the let-bindings. Otherwise, execute 
ELSE within the lexical scope of the let-bindings."
  `(let ,bindings
     (if (and ,@(mapcar 'car bindings))
         ,then
       ,else)))


(defmacro let-when (bindings &rest body)
  "First, evaluate the BINDINGS. If all of them are non-nil, execute 
BODY within the lexical scope of the let-bindings."
  `(let-if ,bindings
           (progn
             ,@body)))


(defun every-nth (n sequence &optional from to)
  "Return every Nth element from SEQUENCE"
  (when sequence
    (let* ((len (length sequence))
           ;; bound from/to to be the boundaries of the sequence
           (from (min len (max 0 (or from 0))))
           (to (min len (min len (or to len))))
           (count (ceiling (- to from) n))
           (result (cond
                    ((vectorp sequence) (make-vector count nil))
                    (t (make-list count nil)))))
      (dotimes (ridx count result)
        (let ((sidx (+ (* n ridx) from)))
          (setf (elt result ridx) (elt sequence sidx)))))))


(defun every-other (sequence &optional from to)
  "Return a sequence, containing every other value in SEQUENCE, starting at FROM, up to TO"
  (every-nth 2 sequence from to))


(defun here-or-back-files? (file-name)
  (or (string= file-name ".") (string= file-name "..")))


(defun --utils-directory-files-recursively (directory regexp &optional include-directories)
  ""
  (let ((results '()))
    (nlet do-recurse ((dir directory))
      (let ((contents (directory-files dir nil regexp)))
        (dolist (file contents)
          (unless (here-or-back-files? file)
            (let ((full-path (concat directory file)))
              (print file)
              (if (file-directory-p full-path)
                  (progn
                    (appendf results (do-recurse full-path))
                    (when include-directories
                      (push full-path results)))
                (push full-path results)))))
        results))))


(eval-and-compile
  (when (<= emacs-major-version 24)
    (defalias 'directory-files-recursively '--utils-directory-files-recursively)))


(defmacro while-let (binding &rest body)
  (let ((recurse (gensym)))
    `(nlet ,recurse ,binding
       (when ,(caar binding)
         ,@body
         (,recurse ,(cadar binding)))))) 


(defmacro while-any (bindings &rest body)
  (let ((recurse (gensym)))
    `(nlet ,recurse ,bindings
       (when (or ,@(mapcar 'car bindings))
         ,@body
         (,recurse ,@(mapcar 'cadr bindings))))))


(defmacro while-all (bindings &rest body)
  (let ((recurse (gensym)))
    `(nlet ,recurse ,bindings
       (when (and ,@(mapcar 'car bindings))
         ,@body
         (,recurse ,@(mapcar 'cadr bindings))))))


(provide 'utils)
