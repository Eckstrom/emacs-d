                                        ; init.el

;; Author: Joseph Eckstrom
;; Version: 2019.01.30 (originally 2015.07.08)
;; Permission is hereby granted, free of charge, to any person obtaining a
;; copy of this software and associated documentation files (the "Software"),
;; to deal in the Software without restriction, including without limitation
;; the rights to use, copy, modify, merge, publish, distribute, sublicense,
;; and/or sell copies of the Software, and to permit persons to whom the
;; Software is furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included
;; in all copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;; OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;; ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;; OTHER DEALINGS IN THE SOFTWARE.

;;; Commentary:

;; This is my personal startup file for GNU Emacs.
;; Based on Andrew Kensler's init.el (https://www.cs.utah.edu/~aek/code/init.el.html)
;; Shamelessly stolen from sources far-and-wide. If you notice something that's
;; yours, I didn't give you credit, and you want credit, then I'm sorry - just let me know.

;; Miscellaneous settings & Requirements
;; -------------------------------------
;;

;; Debug?
                                        ;(setq debug-on-error t)

(setq package-enable-at-startup nil)
(add-to-list 'load-path (expand-file-name "~/.emacs.d/custom"))
(byte-recompile-directory (expand-file-name "~/.emacs.d/custom") 0)

(require 'find)
(require 'utils)
(require 'server)
(require 'uniquify)
(require 'font-lock)
(require 'auto-header)

(setq-default ;; highlight ALL THE THINGS!
 font-lock-user-fonts '(or (mono) (grayscale))
 font-lock-use-colors '(color)
 font-lock-maximum-decoration t
 font-lock-auto-fontify t
 font-lock-maximum-size nil
 font-lock-auto-fontify t
 ;; get rid of annoying warnings on start
 display-warning-minimum-level 'error
 ;; no more accidental exits
 confirm-kill-emacs 'y-or-n-p
 ;; show both lines and columns
 line-number-mode t
 column-number-mode t
 ;; bar cursor if possible (probably not on Windows)
 cursor-type 'bar
 ;; spaces instead of tabs everywhere
 indent-tabs-mode nil
 ;; uniquify buffer if there is a name clash
 uniquify-buffer-name-style 'forward
 uniquify-after-kill-buffer-p t
 uniquify-ignore-buffers-re "^\\*"
 ;; show diffs side-by-side
 ediff-split-window-functon 'split-window-horizontally
 ;; GDB in all its glory
 gdb-many-windows t
 ;; org-mode options
 org-support-shift-select t
 org-catch-invisible-edits t)

;; line numbers on files
(setq linum-mode t
      global-linum-mode t)

;; set backup file configurations
(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5    ; and how many of the old
      )

;; Org-Mode agenda files for scheduling and TODOs
(setq org-agenda-files '("~/org/tickets.org"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(package-selected-packages
   (quote
    (magit magit-diff-flycheck magithub exec-path-from-shell elixir-yasnippets flycheck-credo flycheck-dialyxir flycheck-dogma eglot lsp-elixir\.el dap-mode flycheck-mix lsp-elixir elixir-mode flycheck-elixir ample-theme cider clojure-mode d-mode company omnisharp znc slime 2048-game)))
 '(safe-local-variable-values
   (quote
    ((cg . "1.103.2.10")
     (lisp-version . "8.1 [Windows] (Apr 15, 2008 21:33)")))))

;; give me a random, QOTD at startup
(require 'emacs-fortune)
(defun startup-echo-area-message ()
  (emacs-fortune))

;; overwrite the selcted region when I start typing
(delete-selection-mode t)

;; replace full "yes" or "no" answers with just "y" or "n"
(fset 'yes-or-no-p 'y-or-n-p)
;; start the emacs server by default.
(if (not (server-running-p))
    (server-start))

;;; MELPA settings
;;proxy
;;(setq url-proxy-services
;;      '(("http"  . "proxy-url.proxy.com:port")
;;        ("https" . "proxy-url.proxy.com:port")))

(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t))


(require 'cl-lib)
(defvar *my-packages*
  '(2048-game slime cider clojure-mode))

;; initialize the package management system
(package-initialize)

;; install missing packages
((lambda ()
   "Ensure the packages I use are installed. see *my-packages*"
   (interactive)
   (unless package-archive-contents
     (package-refresh-contents))
   (dolist (package *my-packages*)
     (unless (package-installed-p package)
       (package-install package)))))

(when (string-equal system-type "darwin")
  (package-install 'exec-path-from-shell)
  (exec-path-from-shell-initialize))



;; Font lock customization
;; -----------------------
;; This is an attempt to provide a pleasing, sane and resonably consistent
;; color scheme across GNU Emacs, XEmacs, Windows, X, and TTY systems. It
;; also highlights fixme type tags.
(global-font-lock-mode t)
(add-hook 'font-lock-mode-hook 'font-lock-mode-setup)
(defun font-lock-mode-setup ()
  "Make fixme tags standout."
  (font-lock-add-keywords
   nil '(("\\<\\(JTE\\|JECKSTROM\\|NOTE\\|FIXME\\)\\>:?"
          0 'font-lock-warning-face prepend))))

;; custom themes
(load-theme 'ample t t)
(enable-theme 'ample)

;; Extra key bindings
;; ------------------

;; information:
;; c-z and c-; as well as free c-c bindings, are a user-defined prefixes
;;
;; C-; C-;     comments or uncomments the region or line
;; C-; l       maps org-store-link anywhere
;; C-; b       maps org-iswitchb anywhere
;; C-; c       maps org-capture anywhere
;; C-; a       maps org-agenda anywhere
;;
;; C-c j       kill the current line, without copying it
;; C-c k       kill the current line, copying it
;; C-c l       kill the previous N lines (copying)
;; C-z j       gives a fortune :-)
;; C-c ;       kill the next N lines (copying)
;; C-c >       kill from the cursor to the end of the line
;; C-c <       kill from the cursor to the beginning of the line
;; C-c v       paste/yank what's in the kill-ring above the cursor
;; C-c b       paste/yank what's in the kill-ring below the cursor
;; C-c p       paste/yank what's in the kill-ring n lines above or below the cursor
;;
;; C-z |       aligns the region
;; C-z TAB     indent the entire file according to the rules of the major mode
;; C-z l       sorts the lines in the region
;; C-z k       kill all other open buffes
;; C-z n       opens a new, empty file
;; C-z t       deletes trailing whitespace
;; C-z "       place the word or selection in quotes
;; C-z C-"     quote each individual word in the selection or current line
;; C-z ?       filter lines containing a provided regex
;; C-z =       sort the lines and delete any duplicates
;; C-z /       Cut out the first "field" of the line or every line in the region
;; C-z C-/
;;
;; M-g         uses X-Emacs goto-line in GNU
;; M-G         goto char
;; M-5         query-replace on current line
;; C-h a       apropos symbols and commands
;; C-x C-b     remapped to use ibuffer
;; kill-region has been remapped so that it kills the line if no region is selected
;;
(require 'emacs-keybindings)
(define-key global-map [(control ?z)] nil)                    ; Commandeer C-z and C-; for personal prefix keys
(define-key global-map [(control ?\;)] nil)

;; C-z keybindings
(define-key global-map [(control ?z) ?|] 'align-current)      ; Note: try space before first entry then C-u C-z |
(define-key global-map [(control ?z) tab] 'indent-whole-buffer)
(define-key global-map [(control ?z) ?l] 'sort-lines)
(define-key global-map [(control ?z) ?t] 'delete-trailing-whitespace)
(define-key global-map [(control ?z) ?j] 'tell-fortune)
(define-key global-map [(control ?z) ?k] 'kill-other-buffers)
(define-key global-map [(control ?z) ?n] 'make-new-empty-file)
(define-key global-map [(control ?z) ?\"] 'quotify-boundaries)
(define-key global-map [(control ?z) (control ?\")] 'quotify-each-word)
(define-key global-map [(control ?z) ?\'] (lambda ()
                                            (interactive)
                                            (quotify-boundaries t)))
(define-key global-map [(control ?z) (control ?\')] (lambda ()
                                                      (interactive)
                                                      (quotify-each-word t)))
(define-key global-map [(control ?z) ?\?] 'keep-matching-lines)
(define-key global-map [(control ?z) ?\=] 'keep-unique-lines)


;; C-c keybindings
(define-key global-map [(control meta shift ?j)] 'goto-new-line-above)
(define-key global-map [(control shift ?j)] 'open-line-above)
(define-key global-map [(control ?c) ?j] 'kill-line-nocopy)
(define-key global-map [(control ?c) ?k] 'kill-line)
(define-key global-map [(control ?c) ?l] 'kill-previous-lines)
(define-key global-map [(control ?c) ?\;] 'kill-next-lines)
(define-key global-map [(control ?c) ?\<] 'kill-to-beginning)
(define-key global-map [(control ?c) ?\>] 'kill-to-end)
(define-key global-map [(control ?c) ?v] 'paste-above)
(define-key global-map [(control ?c) ?b] 'paste-below)
(define-key global-map [(control ?c) ?p] 'paste-at)
(define-key global-map [(control ?c) ?g] 'revert-buffer-noconfirm)
(define-key global-map [(control ?c) (shift ?g)] (lambda ()
                                                   (interactive)
                                                   (dolist (buffer (buffer-list))
                                                     (with-current-buffer buffer
                                                       (revert-buffer-noconfirm)))))


;; C-; keybindings
;; custom
(define-key global-map [(control ?\;) (control ?\;)] 'comment-or-uncomment)

;; globaly-available org-mode commands
(define-key global-map [(control ?\;) ?l] 'org-store-link)
(define-key global-map [(control ?\;) ?b] 'org-iswitchb)
(define-key global-map [(control ?\;) ?c] 'org-capture)
(define-key global-map [(control ?\;) ?a] 'org-agenda)

;; Buffer commands
(define-key global-map [(control tab)] 'bury-buffer)
(define-key global-map [(control ?x) (control ?b)] 'ibuffer)

;; Use XEmacs' M-g for goto-line in GNU too
(define-key global-map [(meta ?g)] 'goto-line)
(define-key global-map [(meta shift ?g)] 'goto-char)

;; Use C-: for replacing text on only the current line
(define-key global-map [(control ?\:)] (lambda (from-string to-string &optional delimited start end)
                                         (interactive
                                          (let ((common
                                                 (query-replace-read-args ;; see replace.el
                                                  (concat "Query replace"
                                                          (if current-prefix-arg
                                                              (if (eq current-prefix-arg '-) " backward" " word")
		                                            ""))
                                                  nil)))
                                            (list (nth 0 common)
                                                  (nth 1 common))))
                                         (query-replace from-string
                                                        to-string
                                                        nil
                                                        (line-beginning-position)
                                                        (line-end-position))))
;; OS X "fix"
(define-key global-map [end] 'end-of-line)
(define-key global-map [home] 'beginning-of-line)

;; Apropos *all* symbols, not just commands
(define-key global-map [(control ?h) ?a] 'apropos)

;; Custom-remapping of 'kill-region
(define-key global-map [remap kill-region] 'kill-region-rmp)

;; When I 'write-file' on an existing file, I usually mean "move file"
(define-key global-map [remap write-file] 'rename-current-buffer-file)

;; When I'm in a comment, use C-RET for indent-new-comment-line
;; Then use C-M-RET to close and escape the comment
(define-key global-map [(control return)] (kbd "M-j"))

;; Org Settings
(setq org-todo-keywords
      '((sequence "TODO(t)" "PENDING(p)" "|" "DONE(d)" "CANCELED(c)")
        (type "SoSC(s)" "|" "DONE(d)")))

(setq org-todo-keyword-faces
      '(("TODO" . org-warning) ("PENDING" . org-warning)
        ("CANCELED" . (:foreground "blue" :weight bold))))

;;; Mode Hooks
(defconst joey-c-common-style
  '('stroustrup
    (c-basic-offset . 8)
    (c-offsets-alist . ((innamespace . 0)
                        (inlambda . 0)
                        (inclass . +)
                        (inline-open 0)
                        ))))


(c-add-style "joey-c-common" joey-c-common-style)

(defun joey-c-mode-settings ()
  (c-set-style "joey-c-common")
  (setq tab-width        8
        indent-tabs-mode nil)
  (define-key c-mode-base-map [return] 'newline-and-indent)
  (add-hook 'before-save-hook 'update-date-information))


(defun joey-d-mode-settings ()
  (c-set-style "joey-c-common")
  (setq tab-width          4
        indent-tabs-mode nil)
  (define-key d-mode-base-map [return] 'newline-and-indent)
  (add-hook 'before-save-hook 'update-date-information))


(defun joey-lisp-mode-settings ())

;;;; ELIXIR ;;;;
(require 'flycheck-mix)
(require 'flycheck-credo)
(require 'flycheck-dialyxir)

(eval-after-load 'flycheck
  '(progn
     (flycheck-credo-setup)
     (flycheck-dialyxir-setup)
     (flycheck-mix-setup)))
(add-hook 'elixir-mode-hook (lambda ()
                              (progn
                                (setq flycheck-elixir-credo-strict t)
                                (flycheck-mode)
                                (add-hook 'before-save-hook 'delete-trailing-whitespace)
                                (define-key elixir-mode-map [return] 'newline-and-indent))))

(flycheck-add-next-checker 'elixir-mix 'elixir-credo)
(flycheck-add-next-checker 'elixir-credo 'elixir-dialyxir)


;; Mode hooks
(add-hook 'c-common-mode-hook 'joey-c-mode-settings)
(add-hook 'c++-mode-hook 'joey-c-mode-settings)
(add-hook 'lisp-mode-hook 'joey-lisp-mode-settings)
(add-hook 'emacs-lisp-mode-hook 'joey-lisp-mode-settings)
(add-hook 'd-mode-hook 'joey-d-mode-settings)
(add-to-list 'auto-mode-alist '("\\.c\\'"   . c-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'"   . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.d\\'"   . d-mode))

;;; SLIME
(setq inferior-lisp-program "/usr/local/bin/ccl64")
(setq slime-contribs '(slime-fancy))


;;; Custom set faces
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; Set the default "find file" location on Windows
(eval-and-compile
  (when (eq system-type 'windows-nt)
    (setq default-directory (concat "/Users/" user-login-name "/"))))
(cd default-directory)

;; Optional local configuration which may modify or overwrite anything
;; previously presented in this file.
;; Be aware that this file is *not* saved to github, as it's meant to be
;; a local-only configuration
(let ((local (expand-file-name "~/.emacs.d/local.el")))
  (when (file-exists-p local)
    (byte-compile-file local t)))
